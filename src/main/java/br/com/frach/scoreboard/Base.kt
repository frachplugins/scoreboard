package br.com.frach.scoreboard

import br.com.frach.scoreboard.engines.ScoreboardEngine
import br.com.frach.scoreboard.utils.scheduler
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.java.JavaPlugin
import org.jetbrains.exposed.sql.Database

class Base: JavaPlugin(), Listener {

    companion object {
        lateinit var dataSource: Database
        @JvmStatic lateinit var instance: Base
        lateinit var scoreboardManager: ScoreboardEngine
    }

    override fun onEnable() {

        instance = this

        saveDefaultConfig()

        scoreboardManager = ScoreboardEngine()

        server.pluginManager.registerEvents(this, this)

        println("Plugin ativado com sucesso.")

        scheduler {
            scoreboardManager.playerScoreboards.values.forEach { it.update() }
        }.runTaskTimer(this, 0L, 20L)

    }


    @EventHandler
    fun join(e: PlayerJoinEvent){
        scoreboardManager.addToPlayerCache(e.player)
    }

    @EventHandler
    fun quit(e: PlayerQuitEvent){
        scoreboardManager.removeFromPlayerCache(e.player)
    }

}