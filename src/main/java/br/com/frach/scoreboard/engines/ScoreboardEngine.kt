package br.com.frach.scoreboard.engines

import br.com.frach.scoreboard.objects.DefaultScoreboard
import br.com.frach.scoreboard.utils.scoreboard.PlayerScoreboard
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.event.Listener

import java.util.concurrent.ConcurrentHashMap

class ScoreboardEngine : Listener {

    private val scoreboards: ConcurrentHashMap<Player, PlayerScoreboard>

    val playerScoreboards: ConcurrentHashMap<Player, PlayerScoreboard>
        get() = this.scoreboards

    init {
        this.scoreboards = ConcurrentHashMap()

        Bukkit.getOnlinePlayers().forEach { addToPlayerCache(it) }
    }

    fun hasCachedPlayerScoreboard(player: Player): Boolean {
        if(scoreboards.containsKey(player)){
            return scoreboards[player]!!.scoreboard != null
        }
        return false
    }

    fun addToPlayerCache(player: Player) {
        if (!this.hasCachedPlayerScoreboard(player)) {
            val provider = DefaultScoreboard()
            if (provider.getTitle(player) == null || provider.getLines(player) == null) {
                return
            }
            this.scoreboards[player] = PlayerScoreboard(provider, player)
        }
    }

    fun removeFromPlayerCache(player: Player) {
        if (this.hasCachedPlayerScoreboard(player)) {
            this.scoreboards.remove(player)!!.disappear()
        }
    }

    fun getPlayerScoreboard(player: Player): PlayerScoreboard {
        if (!this.hasCachedPlayerScoreboard(player)) this.addToPlayerCache(player)

        return this.scoreboards.get(player)!!
    }

}