package br.com.frach.scoreboard.objects

import br.com.frach.scoreboard.Base
import br.com.frach.scoreboard.utils.scoreboard.ScoreboardProvider
import br.com.frach.scoreboard.utils.scoreboard.ScoreboardText
import com.google.common.collect.Lists
import me.clip.placeholderapi.PlaceholderAPI
import net.sacredlabyrinth.phaed.simpleclans.SimpleClans
import org.bukkit.ChatColor
import org.bukkit.entity.Player

import java.util.HashMap

class DefaultScoreboard : ScoreboardProvider() {

    private val defaultProvider: HashMap<ProviderType, ScoreboardProvider>

    init {
        this.defaultProvider = HashMap()

        this.defaultProvider[ProviderType.SCOREBOARD] = FantasyScoreboard()
    }

    override fun getTitle(player: Player): String? {
        return this.defaultProvider[ProviderType.SCOREBOARD]!!.getTitle(player)
    }

    override fun getLines(player: Player): List<ScoreboardText>? {
        return this.defaultProvider[ProviderType.SCOREBOARD]!!.getLines(player)
    }

    enum class ProviderType {

        SCOREBOARD

    }

    private inner class FantasyScoreboard : ScoreboardProvider() {

        override fun getTitle(player: Player): String {
            return ChatColor.translateAlternateColorCodes('&',
                    Base.instance.config.getString("Scores.Title")
            )
        }

        override fun getLines(player: Player): List<ScoreboardText> {

            val lines = Lists.newArrayList<ScoreboardText>()

            val clanPlayer = SimpleClans.getInstance().clanManager.getClanPlayer(player)

            val listLines = if(clanPlayer == null) Base.instance.config.getStringList("Scores.SEM_CLAN") else Base.instance.config.getStringList("Scores.COM_CLAN")

            listLines.forEach {
                lines.add(ScoreboardText(
                        ChatColor.translateAlternateColorCodes('&',
                                PlaceholderAPI.setPlaceholders(player, it)
                        )
                ))
            }

            return lines

        }

    }

}
