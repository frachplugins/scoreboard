package br.com.frach.scoreboard.utils.scoreboard;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.List;

public class PlayerScoreboard {

    private ScoreboardProvider provider;

    private Player player;

    private Scoreboard scoreboard;
    private Objective objective;

    private boolean active;
    private int lastSentCount = -1;

    public PlayerScoreboard(ScoreboardProvider provider, Player player){
        this.provider = provider;

        if(provider.getTitle(player) == null || provider.getLines(player) == null){
            return;
        }

//        if(this.provider.getTitle(player) != null){
//            Preconditions.checkState(this.provider.getTitle(player).length() <= 32, "Title can't be more than 32 chars.");
//        }

        this.player = player;

        this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        this.objective = this.getOrCreateObject(this.provider.getTitle(player));
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        this.player.setScoreboard(this.scoreboard);

        this.active = true;

        this.update();
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public void setProvider(ScoreboardProvider provider) {
        this.provider = provider;
    }

    public Objective getOrCreateObject(String objective){
        Objective value = this.scoreboard.getObjective("dummyBoard");

        if(value == null){
            value = this.scoreboard.registerNewObjective("dummyBoard", "dummy");
        }
        value.setDisplayName(objective);
        return value;
    }

    public void disappear(){
        if(this.active && this.scoreboard != null){
            this.scoreboard.getTeams().forEach((team) -> team.unregister());
            this.scoreboard.getObjectives().forEach((obj) -> obj.unregister());
        }
    }

    public void remove(int index){
        if(!this.active){
            return;
        }

        String name = this.getNameForIndex(index);

        this.scoreboard.resetScores(name);

        Team team = this.getOrCreateTeam(ChatColor.stripColor(StringUtils.left(this.provider.getTitle(this.player), 14)) + index, index);
        team.unregister();
    }

    public void update(){
        if(!this.active){
            return;
        }
        String title = this.provider.getTitle(this.player);
        List<ScoreboardText> lines = this.provider.getLines(this.player);

        if(this.objective.getDisplaySlot() == DisplaySlot.SIDEBAR){
            this.objective.setDisplayName(title);
        }

        for (int i = 0; i < lines.size(); i++) {
            Team team = this.getOrCreateTeam(ChatColor.stripColor(StringUtils.left(title, 14)) + i, i);

            ScoreboardText text = lines.get(lines.size() - i - 1);

            team.setPrefix(text.getText());
            team.setSuffix(ChatColor.getLastColors(text.getText()) + text.getExtentedText());

            this.objective.getScore(this.getNameForIndex(i)).setScore(i + 1);
        }

        if(this.lastSentCount != -1){
            for (int i = 0; i < this.lastSentCount - lines.size(); i++) {
                this.remove(lines.size() + i);
            }
        }

        this.lastSentCount = lines.size();

    }

    public Team getOrCreateTeam(String team, int i){
        Team value = this.scoreboard.getTeam(team);

        if(value == null){
            value = this.scoreboard.registerNewTeam(team);
            value.addEntry(this.getNameForIndex(i));
        }
        return value;
    }

    public String getNameForIndex(int index){
        return ChatColor.values()[index].toString() + ChatColor.RESET;
    }

    public Player getPlayer() {
        return player;
    }

    public ScoreboardProvider getProvider() {
        return provider;
    }

    public boolean isActive() {
        return active;
    }

}