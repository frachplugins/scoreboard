package br.com.frach.scoreboard.utils.scoreboard;

import org.apache.commons.lang3.StringUtils;

public class ScoreboardText {

    private String text;
    private String extentedText;

    private boolean extended = false;

    public ScoreboardText(String text){
        // Preconditions.checkState(text.length() <= 32, "Text can't be longer than 32 chars.");
        if(text.length() > 16){
            this.extended = true;

            this.extentedText = text.substring(16);
            this.text = text.substring(0, 16);
        } else {
            this.text = text;
            this.extentedText = StringUtils.EMPTY;
        }
    }

    public String getExtentedText() {
        return extentedText;
    }

    public String getText() {
        return text;
    }

    public boolean isExtended() {
        return extended;
    }

}