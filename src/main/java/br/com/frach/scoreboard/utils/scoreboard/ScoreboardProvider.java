package br.com.frach.scoreboard.utils.scoreboard;

import org.bukkit.entity.Player;

import java.util.List;

public abstract class ScoreboardProvider {
    public abstract String getTitle(Player player);

    public abstract List<ScoreboardText> getLines(Player p);

}